# GitLab CI template for Lighthouse

This project implements a GitLab CI/CD template to continuously analyse your web apps and web pages performances and developer best practices with [Lighthouse CI](https://github.com/GoogleChrome/lighthouse-ci).

## Usage

This template can be used both as a [CI/CD component](https://docs.gitlab.com/ee/ci/components/#use-a-component-in-a-cicd-configuration) 
or using the legacy [`include:project`](https://docs.gitlab.com/ee/ci/yaml/index.html#includeproject) syntax.

### Use as a CI/CD component

Add the following to your `gitlab-ci.yml`:

```yaml
include:
  # 1: include the component
  - component: gitlab.com/to-be-continuous/lighthouse/gitlab-ci-lighthouse@1.3.0
    # 2: set/override component inputs
    inputs:
      review-enabled: true # ⚠ this is only an example
```

### Use as a CI/CD template (legacy)

Add the following to your `gitlab-ci.yml`:

```yaml
include:
  # 1: include the template
  - project: 'to-be-continuous/lighthouse'
    ref: '1.3.0'
    file: '/templates/gitlab-ci-lighthouse.yml'

variables:
  # 2: set/override template variables
  REVIEW_ENABLED: "true" # ⚠ this is only an example
```

## `lighthouse` job

This job runs the [Lighthouse CI](https://github.com/GoogleChrome/lighthouse-ci) analysis.

It uses the following variable:

| Input / Variable | Description                              | Default value     |
| ---------------- | ---------------------------------------- | ----------------- |
| `image` / `LHCI_IMAGE` | The Docker image used to run Lighthouse CI (use [browser images](https://github.com/cypress-io/cypress-docker-images/tree/master/browsers) only). | `registry.hub.docker.com/cypress/browsers:latest`   |
| `version` / `LHCI_VERSION` | Lighthouse CI version to run | `latest` |
| `run-opts` / `LHCI_RUN_OPTS` | Lighthouse CI [autorun options](https://github.com/GoogleChrome/lighthouse-ci/blob/main/docs/configuration.md#autorun) | `--upload.target=filesystem --collect.settings.chromeFlags=\"--no-sandbox\" --collect.url=\"%{environment_url}\"` |
| `review-enabled` / `REVIEW_ENABLED` | Set to `true` to enable Lighthouse tests on review environments (dynamic environments instantiated on development branches) | _none_ (disabled) |

All the rest of your Lighthouse CI configuration shall be defined either as [`LHCI_` environment variables](https://github.com/GoogleChrome/lighthouse-ci/blob/main/docs/configuration.md#environment-variables) 
or in one of the supported [configuration files](https://github.com/GoogleChrome/lighthouse-ci/blob/main/docs/configuration.md#configuration-file),
located at the root of your Git repository.

In addition to a textual report in the console, this job produces the following reports, kept for one day:

| Report                       | Format                       | Usage             |
| ---------------------------- | ---------------------------- | ----------------- |
| `reports/lhci/*.{html|json}` | HTML report(s)               | n/a  |

### `$LHCI_RUN_OPTS` late expansion

The `$LHCI_RUN_OPTS` variable supports a **late variable expansion mechanism** with the `%{somevar}` syntax.

Therefore if an upstream job in the pipeline deployed your code to a server and propagated the deployed server url
as `$environment_url` variable (through a [dotenv artifact](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html#artifactsreportsdotenv)), 
then you can easily run Lighthouse CI against this server, simply by using the `%{environment_url}` syntax in the `$LHCI_RUN_OPTS` variable.

:warning: all _to be continuous_ depoyment templates implement this design. Therefore even purely dynamic environments (such as review
environments) will automatically be propagated to your Lighthouse CI analysis.

If you're not using your own deployment job, you may:

1. implement the common _to be continuous_ design (i.e. propagate the environment base url as `$environment_url` variable through a [dotenv artifact](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html#artifactsreportsdotenv))
2. use any programmatic solution of your choice with a JavaScript-based [configuration file](https://github.com/GoogleChrome/lighthouse-ci/blob/main/docs/configuration.md#configuration-file).
3. explicitly set the `collect.url` option in the `LHCI_RUN_OPTS` variable (hardcoded to a single server),

### Hook scripts

The Lighthouse template supports _optional_ **hook scripts** from your project, located in the root directory to perform additional project-specific logic:

* `pre-lighthouse.sh` is executed **before** running Lighthouse,
* `post-lighthouse.sh` is executed **after** running Lighthouse (whichever the tests status).

## Examples

### Analysing multiple urls

This example demonstrates 2 techniques for a project willing to analyse several urls, also reusing the _to be continuous_ `$environment_url` dynamic variable.

#### Solution 1: `LHCI_RUN_OPTS`-based

The easiest solution is to declare all the analysed urls in the `$LHCI_RUN_OPTS` variable using the late variable expansion mechanism.

```yaml
include:
  - component: gitlab.com/to-be-continuous/lighthouse/gitlab-ci-lighthouse@1.3.0
    inputs:
      run-opts: >- 
    --upload.target=filesystem 
    --collect.settings.chromeFlags="--no-sandbox"
    --collect.url="%{environment_url}"
    --collect.url="%{environment_url}/admin" 
    --collect.url="%{environment_url}/profile"
```

#### Solution 2: Javascript-based

Another option could be to handle it in your JavaScript-based [configuration file](https://github.com/GoogleChrome/lighthouse-ci/blob/main/docs/configuration.md#configuration-file):

`.gitlab-ci.yaml`:

```yaml
include:
  - component: gitlab.com/to-be-continuous/lighthouse/gitlab-ci-lighthouse@1.3.0
    inputs:
      run-opts: --upload.target=filesystem --collect.settings.chromeFlags=--no-sandbox
```

`.lighthouserc.js`:

```js
// use $environment_url if set, else 'http://localhost:4200' (dev)
const baseUrl = process.env.environment_url || "http://localhost:4200";
module.exports = {
  ci: {
    collect: {
      startServerCommand: "npm run serve", // dev only, overridden by template in CI/CD
      url: [
        `${baseUrl}`,
        `${baseUrl}/admin`,
        `${baseUrl}/profile`
      ],
    },
    ...
  },
};
```
