# [1.3.0](https://gitlab.com/to-be-continuous/lighthouse/compare/1.2.1...1.3.0) (2024-05-06)


### Features

* add hook scripts support ([0fe138e](https://gitlab.com/to-be-continuous/lighthouse/commit/0fe138eef942f42f0586cca498b40a94e8fb5833))

## [1.2.1](https://gitlab.com/to-be-continuous/lighthouse/compare/1.2.0...1.2.1) (2024-05-05)


### Bug Fixes

* **workflow:** disable MR pipeline from prod & integ branches ([59fbac8](https://gitlab.com/to-be-continuous/lighthouse/commit/59fbac8fff0e0f44fed509fd870b3558a85c860a))

# [1.2.0](https://gitlab.com/to-be-continuous/lighthouse/compare/1.1.0...1.2.0) (2024-1-27)


### Features

* migrate to CI/CD component ([539d32a](https://gitlab.com/to-be-continuous/lighthouse/commit/539d32a05c24b35aacef25033d19fa05d9733b6b))

# [1.1.0](https://gitlab.com/to-be-continuous/lighthouse/compare/1.0.2...1.1.0) (2023-12-8)


### Features

* use centralized tracking image (gitlab.com) ([5243601](https://gitlab.com/to-be-continuous/lighthouse/commit/52436011eeb8cc0d8f546bfc9f275a745ab833df))

## [1.0.2](https://gitlab.com/to-be-continuous/lighthouse/compare/1.0.1...1.0.2) (2023-12-2)


### Bug Fixes

* envsubst when variable contains a '&' ([f47e0ef](https://gitlab.com/to-be-continuous/lighthouse/commit/f47e0efed26de2575b00eac0457f3a66a4621fc9))

## [1.0.1](https://gitlab.com/to-be-continuous/lighthouse/compare/1.0.0...1.0.1) (2023-10-16)


### Bug Fixes

* declare all TBC stages ([6e5e7e8](https://gitlab.com/to-be-continuous/lighthouse/commit/6e5e7e85bf57052209b05ded6c305a356450f9c3))

# 1.0.0 (2023-06-24)


### Bug Fixes

* add changelog generation ([c426ace](https://gitlab.com/to-be-continuous/lighthouse/commit/c426acec0c0169491e4738d23fe280bb03c84db2))
* bash shell check ([e907d2f](https://gitlab.com/to-be-continuous/lighthouse/commit/e907d2f345c83dcd95ac793f0aae49912a75d32c))
* use master or main as PROD_REF ([1648ea2](https://gitlab.com/to-be-continuous/lighthouse/commit/1648ea27af29b757ad8a2c296d425d46a6ec537b))


### Features

* adaptive pipeline ([376a698](https://gitlab.com/to-be-continuous/lighthouse/commit/376a69870b7ba009a4b7d571dd6726dad59830af))
* add scoped variables support ([4248329](https://gitlab.com/to-be-continuous/lighthouse/commit/424832927a425feef8160512bf2786bcafabdd21))
* configurable tracking image ([8c9266a](https://gitlab.com/to-be-continuous/lighthouse/commit/8c9266ab981ec480c6c433a33028ff1eb2cc80b2))
* initial commit ([b9e7b13](https://gitlab.com/to-be-continuous/lighthouse/commit/b9e7b1301f89c1fd04bbc13300291ad32d863db6))
